var class_tayx_1_1_graphy_1_1_g___graph_shader =
[
    [ "InitializeShader", "class_tayx_1_1_graphy_1_1_g___graph_shader.html#a531ef44ced25f9686183cc7c46ca3920", null ],
    [ "UpdateArray", "class_tayx_1_1_graphy_1_1_g___graph_shader.html#ac039f519ec84d521e44bf8f9b90ff7e8", null ],
    [ "UpdateAverage", "class_tayx_1_1_graphy_1_1_g___graph_shader.html#a67831882b060d17c9d7a754546ef2e6b", null ],
    [ "UpdateColors", "class_tayx_1_1_graphy_1_1_g___graph_shader.html#ac4ec72181f804fc7e8516292bd3a5995", null ],
    [ "UpdatePoints", "class_tayx_1_1_graphy_1_1_g___graph_shader.html#a2a5905cd3809406d645ef762f86d9280", null ],
    [ "UpdateThresholds", "class_tayx_1_1_graphy_1_1_g___graph_shader.html#af57c3227f9ad5e7a55f17532749cbf18", null ],
    [ "Array", "class_tayx_1_1_graphy_1_1_g___graph_shader.html#ac7f20b96326c6f961a068e6f537cea6b", null ],
    [ "ArrayMaxSize", "class_tayx_1_1_graphy_1_1_g___graph_shader.html#a4cffc15e668a4a2fe9a1feb43e8a1f23", null ],
    [ "ArrayMaxSizeFull", "class_tayx_1_1_graphy_1_1_g___graph_shader.html#ad490cc631ecd76320d62ee010bfeb230", null ],
    [ "ArrayMaxSizeLight", "class_tayx_1_1_graphy_1_1_g___graph_shader.html#af2db6a5c7a732b406828d34e825becb5", null ],
    [ "Average", "class_tayx_1_1_graphy_1_1_g___graph_shader.html#aef4432a1b54cc6c61e764d65959b1abf", null ],
    [ "CautionColor", "class_tayx_1_1_graphy_1_1_g___graph_shader.html#a7134f6f44dd47258a2807e507111fa94", null ],
    [ "CautionThreshold", "class_tayx_1_1_graphy_1_1_g___graph_shader.html#a31f89409e8b9222827d939855bdb0f89", null ],
    [ "CriticalColor", "class_tayx_1_1_graphy_1_1_g___graph_shader.html#a883bfd3b11a96624db44c036970ea558", null ],
    [ "GoodColor", "class_tayx_1_1_graphy_1_1_g___graph_shader.html#a6d8afecd7ce8a7d2cadb497d87087858", null ],
    [ "GoodThreshold", "class_tayx_1_1_graphy_1_1_g___graph_shader.html#a6555112dd144106004e0d24535296586", null ],
    [ "Image", "class_tayx_1_1_graphy_1_1_g___graph_shader.html#a942c58142cd3cdb952dca1025fabfb77", null ]
];