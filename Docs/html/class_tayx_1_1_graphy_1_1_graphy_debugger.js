var class_tayx_1_1_graphy_1_1_graphy_debugger =
[
    [ "DebugCondition", "struct_tayx_1_1_graphy_1_1_graphy_debugger_1_1_debug_condition.html", "struct_tayx_1_1_graphy_1_1_graphy_debugger_1_1_debug_condition" ],
    [ "DebugPacket", "class_tayx_1_1_graphy_1_1_graphy_debugger_1_1_debug_packet.html", "class_tayx_1_1_graphy_1_1_graphy_debugger_1_1_debug_packet" ],
    [ "ConditionEvaluation", "class_tayx_1_1_graphy_1_1_graphy_debugger.html#a65e6e87ce257443cc98327f4dfdfbc24", [
      [ "All_conditions_must_be_met", "class_tayx_1_1_graphy_1_1_graphy_debugger.html#a65e6e87ce257443cc98327f4dfdfbc24a6058a00ecbde44b7aa131adbcd01f157", null ],
      [ "Only_one_condition_has_to_be_met", "class_tayx_1_1_graphy_1_1_graphy_debugger.html#a65e6e87ce257443cc98327f4dfdfbc24a43bbd46bded07e6f823b50e3eeb77118", null ]
    ] ],
    [ "DebugComparer", "class_tayx_1_1_graphy_1_1_graphy_debugger.html#a12a476a36ed77861c3afcd027cfa87aa", [
      [ "Less_than", "class_tayx_1_1_graphy_1_1_graphy_debugger.html#a12a476a36ed77861c3afcd027cfa87aaa8a7ab52bef8fa338c5c78c6575f90d67", null ],
      [ "Equals_or_less_than", "class_tayx_1_1_graphy_1_1_graphy_debugger.html#a12a476a36ed77861c3afcd027cfa87aaa9e1d0a9d15dc800c3c847ebf16ce1773", null ],
      [ "Equals", "class_tayx_1_1_graphy_1_1_graphy_debugger.html#a12a476a36ed77861c3afcd027cfa87aaa0ccb67e7eaae09d9e4078d161eeca100", null ],
      [ "Equals_or_greater_than", "class_tayx_1_1_graphy_1_1_graphy_debugger.html#a12a476a36ed77861c3afcd027cfa87aaaaf2044638c4a9006a56b5d7ffc7d9186", null ],
      [ "Greater_than", "class_tayx_1_1_graphy_1_1_graphy_debugger.html#a12a476a36ed77861c3afcd027cfa87aaa66c6ff7493a9289820718efdfe6d1336", null ]
    ] ],
    [ "DebugVariable", "class_tayx_1_1_graphy_1_1_graphy_debugger.html#a498e54569d3621d1a3b0d94abdf2e173", [
      [ "Fps", "class_tayx_1_1_graphy_1_1_graphy_debugger.html#a498e54569d3621d1a3b0d94abdf2e173a1b6a4f07de79256966f51a67aa2fd834", null ],
      [ "Fps_Min", "class_tayx_1_1_graphy_1_1_graphy_debugger.html#a498e54569d3621d1a3b0d94abdf2e173a9b69014f1529e3703ee866fb168a3335", null ],
      [ "Fps_Max", "class_tayx_1_1_graphy_1_1_graphy_debugger.html#a498e54569d3621d1a3b0d94abdf2e173a90c6f2987ddd6df4135a4a60b80ed5e5", null ],
      [ "Fps_Avg", "class_tayx_1_1_graphy_1_1_graphy_debugger.html#a498e54569d3621d1a3b0d94abdf2e173aa86786bbebfe8bed22a377bc933f111e", null ],
      [ "Ram_Allocated", "class_tayx_1_1_graphy_1_1_graphy_debugger.html#a498e54569d3621d1a3b0d94abdf2e173a974dd6138fcd8845d69f18edb19cb5c7", null ],
      [ "Ram_Reserved", "class_tayx_1_1_graphy_1_1_graphy_debugger.html#a498e54569d3621d1a3b0d94abdf2e173a91e1f4d99db72dc9e8efb0798c835b8c", null ],
      [ "Ram_Mono", "class_tayx_1_1_graphy_1_1_graphy_debugger.html#a498e54569d3621d1a3b0d94abdf2e173acfb844db6d0eea1534b3c9bf4b357f3d", null ],
      [ "Audio_DB", "class_tayx_1_1_graphy_1_1_graphy_debugger.html#a498e54569d3621d1a3b0d94abdf2e173afb6703ffd0a228e68bd9cb05f4d330aa", null ]
    ] ],
    [ "MessageType", "class_tayx_1_1_graphy_1_1_graphy_debugger.html#a4a7adc23010d0326a1ad9df4b0608fb9", [
      [ "Log", "class_tayx_1_1_graphy_1_1_graphy_debugger.html#a4a7adc23010d0326a1ad9df4b0608fb9ace0be71e33226e4c1db2bcea5959f16b", null ],
      [ "Warning", "class_tayx_1_1_graphy_1_1_graphy_debugger.html#a4a7adc23010d0326a1ad9df4b0608fb9a0eaadb4fcb48a0a0ed7bc9868be9fbaa", null ],
      [ "Error", "class_tayx_1_1_graphy_1_1_graphy_debugger.html#a4a7adc23010d0326a1ad9df4b0608fb9a902b0d55fddef6f8d651fe1035b7d4bd", null ]
    ] ],
    [ "GraphyDebugger", "class_tayx_1_1_graphy_1_1_graphy_debugger.html#a1d30de6e49852e846b44bf2c213db913", null ],
    [ "AddCallbackToAllDebugPacketWithId", "class_tayx_1_1_graphy_1_1_graphy_debugger.html#a7bf1c6637c7a6bcc52d4a82d8be5dc23", null ],
    [ "AddCallbackToFirstDebugPacketWithId", "class_tayx_1_1_graphy_1_1_graphy_debugger.html#a67ac3413b22c7dffadfb404b2fb300f5", null ],
    [ "AddNewDebugPacket", "class_tayx_1_1_graphy_1_1_graphy_debugger.html#ac931fb22cc7eb31f5bdd02cdbf8383fd", null ],
    [ "AddNewDebugPacket", "class_tayx_1_1_graphy_1_1_graphy_debugger.html#a8c2fdcac07cae9a7e754335853b11cd8", null ],
    [ "AddNewDebugPacket", "class_tayx_1_1_graphy_1_1_graphy_debugger.html#a0c8c63d460f9c1234f2fcd0d04d40ace", null ],
    [ "AddNewDebugPacket", "class_tayx_1_1_graphy_1_1_graphy_debugger.html#acaa3c19845ccecf9ac9a8839c09fedf5", null ],
    [ "AddNewDebugPacket", "class_tayx_1_1_graphy_1_1_graphy_debugger.html#a360734cc3cd2d4ca5bfed30a6c7db91c", null ],
    [ "GetAllDebugPacketsWithId", "class_tayx_1_1_graphy_1_1_graphy_debugger.html#ac0ef7ff49e62d1f88947db9592c4943a", null ],
    [ "GetFirstDebugPacketWithId", "class_tayx_1_1_graphy_1_1_graphy_debugger.html#ac685ec11a8d70facfa737dd43b8f1df9", null ],
    [ "RemoveAllDebugPacketsWithId", "class_tayx_1_1_graphy_1_1_graphy_debugger.html#a023f27db1b263ce298906d6e39fd348a", null ],
    [ "RemoveFirstDebugPacketWithId", "class_tayx_1_1_graphy_1_1_graphy_debugger.html#ae769f3e4aa77266c4bb116540b967e03", null ]
];